# Save System Package
A simple example to save your data into JSON.

## Requirements
* Unity 2020.3 or later.

## Dependencies
* ```"com.unity.addressables": "1.19.19"```
* ```"com.bourdonvalentin.scriptable-object-workflow": "https://gitlab.com/valentin_bourdon/ScriptableObject_Workflow.git"```

## Installing the Package
* Add the name of the package (found in package.json) followed by the repository URL to your Unity project's manifest.json or using the package manager in Unity.

```json
{
  "dependencies": {
    "com.bourdonvalentin.inventory-system": "https://gitlab.com/valentin_bourdon/SaveSystem.git",
}
```

## Usage

1. Create a new script who inherit from **SaveSystem.cs**
```
using SaveSystem;

[CreateAssetMenu(fileName = "SaveSystem", menuName = ("SaveSystem"))]
public class GameSave : SaveSystem.SaveSystem{

    //Add your variables/ScriptableObject your want to save

    public override void SaveDataToDisk()
    {
        //SAVE YOUR DATA HERE

        base.SaveDataToDisk();
    }

    public override void LoadData()
    {
        base.LoadData();

        //LOAD YOUR DATA HERE
    }

}

```

2. From other scripts, you can save/load your data's when you want
```

public class StartGame : MonoBehaviour{

  [Header("Save System")]
  [SerializeField] private SaveSystem _saveSystem;
  
   public void Start(){
        //Load
        _saveSystem.LoadData();
   }

   public void OnApplicationQuit(){
     //Save
     _saveSystem.SaveDataToDisk();
   }

}

```
  

## License

All the changes made are released under the MIT License, see [LICENSE](./LICENSE).
