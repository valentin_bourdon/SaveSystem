using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using ScriptableObjectWorkflow.Events;

namespace SaveSystem
{
	/// <summary>
	/// Manages the backup and loading of data.
	/// </summary>

	public class SaveSystem : ScriptableObject
	{
		[Header("LISTENING CHANNELS")]
		[SerializeField] private VoidEventChannelSO _saveSettingsEvent = default;

		private string _saveFilename = "player.save";
		private string _backupSaveFilename = "player.save.bak";
		private Save _saveData = new Save();


		void OnEnable()
		{
			_saveSettingsEvent.OnEventRaised += SaveDataToDisk;
		}

		void OnDisable()
		{
			_saveSettingsEvent.OnEventRaised -= SaveDataToDisk;
		}

		#region SAVE

		public virtual void SaveDataToDisk()
		{
			//SAVE YOUR SPECIFIC DATA HERE


			if (FileManager.MoveFile(_saveFilename, _backupSaveFilename))
			{
				if (FileManager.WriteToFile(_saveFilename, _saveData.ToJson()))
				{
					Debug.Log("<color=green>Save successful " + _saveFilename + "</color>");
				}
			}
		}

		#endregion

		#region LOAD

		private bool LoadSaveDataFromDisk()
		{
			if (FileManager.LoadFromFile(_saveFilename, out var json))
			{
				_saveData.LoadFromJson(json);
				Debug.Log("<color=green>Load successful " + _saveFilename + "</color>");
				return true;
			}

			return false;
		}

		public virtual void LoadData()
		{
			LoadSaveDataFromDisk();

			//LOAD YOUR SPECIFIC DATA HERE
		}

		#endregion

		public void WriteEmptySaveFile()
		{
			FileManager.WriteToFile(_saveFilename, "");
		}
	}
}
	
