using System;
using System.Collections.Generic;
using UnityEngine;

namespace SaveSystem
{
	[Serializable]
	public class Save
	{

		public string ToJson()
		{
			return JsonUtility.ToJson(this);
		}

		public void LoadFromJson(string json)
		{
			try
			{
				JsonUtility.FromJsonOverwrite(json, this);
			}
			catch (Exception e)
			{
				Debug.LogError($"Failed to read from {json} with exception {e}");
			}
		}
	}
}

