# Code Editor Package for Save System


## [0.1.0] - 2022-05-24

### This is the first release of *Unity Package Save System*.

Using the newly created package to integrate Save System with Unity and save your data into JSON.
	- Save
	- SaveSystem
	- FileManager
	- SerializedItemStack
	- README
